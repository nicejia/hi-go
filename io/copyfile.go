package io

import (
	"io"
	"os"
)

//CopyFile 复制文件:字符串到字符串
func CopyFileP1(src string, dist string) (err error) {
	srcFile, err := os.Open(src)
	if err != nil {
		return
	}
	defer srcFile.Close()

	diesFile, err := os.Create(dist)
	if err != nil {
		return
	}
	defer diesFile.Close()

	_, err = io.Copy(diesFile, srcFile)
	return
}

//CopyFile 复制文件:reader到字符串
func CopyFileP2(src io.Reader, dist string) (err error) {
	diesFile, err := os.Create(dist)
	if err != nil {
		return
	}
	defer diesFile.Close()

	_, err = io.Copy(diesFile, src)
	return
}
