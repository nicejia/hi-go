//数字转字符串
package conver

import (
	"strconv"
)

//IntToString int类型转string
func IntToString(source int) (target string) {
	target = strconv.Itoa(source)
	return
}

//Int8ToString int8类型转string
func Int8ToString(source int8) (target string) {
	target = strconv.Itoa(int(source))
	return
}

//Int8ToString int16类型转string
func Int16ToString(source int16) (target string) {
	target = strconv.Itoa(int(source))
	return
}

//Int32ToString int32类型转string
func Int32ToString(source int32) (target string) {
	target = strconv.Itoa(int(source))
	return
}

//Int64ToString int64类型转string
func Int64ToString(source int64) (target string) {
	target = strconv.FormatInt(source, 10)
	return
}
