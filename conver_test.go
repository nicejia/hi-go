package hi_go

import (
	"fmt"
	"reflect"
	"testing"
)

func TestIntToString(t *testing.T) {
	var v int8 = -127

	r := reflect.TypeOf(v)
	fmt.Println(r.Kind().String())
}
